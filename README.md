## I-MAV

## Description

Projet de controle / commande de drone convertible (le mavion) par IA (Reinforcement learning) - Application de divers algo de RL et test de l'approche TD-MPC

Resaerch project on Control and Guidance of MAVION by RL and MPC.

Directories :
    /drone : model of quadrotor uav to compare with mavion
    /mavion : model developped by leandro lustosa [Leandro 2017]
    /mitmav : version of mavion dev by MIT, modeling is similar to mavion but adapted by MIT [MIT 2023]
    /rotations : tool for rotation representation by quaternion and direct cosine matrix



ref : 
- https://www.tdmpc2.com/

