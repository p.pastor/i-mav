import numpy as np
from rotation import *
from scipy.optimize import fsolve
from scipy.integrate import solve_ivp
import yaml

class Mav:

    def __init__(self, file="mav.yaml"):
        stream = open(file, "r")
        data = yaml.load(stream, Loader=yaml.Loader)

        self.NAME = data['NAME']
        self.MASS = data['MASS']
        self.INERTIA = data["INERTIA"]
        self.WINGSPAN = data['WINGSPAN']
        self.AERO = data["AERO"]
        self.PACG = data["PACG"]
        self.a0 = 0
        self.PROP = data["PROP"]
        self.PPCG = data["PPCG"]
        self.aT = -5*np.pi/180


    def motor_thrust(self, dx):
        CT, CN  = self.PROP
        # prop forces computation
        T = CT * dx**2
        # prop moments computation
        N = -np.sign(dx) * CN * dx**2
        return T, N

    def force(self, vel, u):
        # coeff AERO : CL_V(kg/m) CD_V(kg/m) CL_T CD_T CLdelta_V(kg/m) CLdelta_T Cmu_T(m)
        CL_V, CD_V, CL_T, CD_T, CLdelta_V, CLdelta_T, Cmu_T = self.AERO

        a_bar = self.a0 + self.aT
        V = np.linalg.norm(vel)
        dx1, dx2 = u[0:2]
        de1, de2 = u[2:4]

        # Force computation
        # thrust contribution
        T1, N1 = self.motor_thrust(dx1)
        T2, N2 = self.motor_thrust(dx2)
        f_T1 = np.array([np.cos(a_bar)*(1 - CD_T)*T1, 0, np.sin(a_bar)*(CL_T - 1)*T1]) 
        f_T2 = np.array([np.cos(a_bar)*(1 - CD_T)*T2, 0, np.sin(a_bar)*(CL_T - 1)*T2])
        f_thrust =f_T1 + f_T2
        # elevon contribution
        f_de1 = -np.array([0, 0, (np.cos(a_bar)*CLdelta_T*T1 + CLdelta_V*V*vel[0])*de1])
        f_de2 = -np.array([0, 0, (np.cos(a_bar)*CLdelta_T*T2 + CLdelta_V*V*vel[0])*de2])
        f_delta = f_de1 + f_de2
        # wing contribution
        f_wing = -np.array([CD_V*V*vel[0], 0, CL_V*V*vel[2]])
        f_tot = f_thrust + f_delta + f_wing

        # Moment computation
        # torque contribution
        m_torque = np.array([np.cos(self.aT), 0, -np.sin(self.aT)]) * (N1 + N2)
        # thrust/airfoil contribution
        R = np.array([[np.cos(self.a0), 0 , -np.sin(self.a0)], [0, 1, 0], [np.sin(self.a0), 0 , np.cos(self.a0)]])
        M = R@(f_T1 - f_T2)
        m_wing = np.array([-self.PPCG[1] * M[2], Cmu_T * (T1 + T2), self.PPCG[1] * M[0]])

        # elevon contribution
        m_delta = np.array([self.PACG[1] * np.cos(self.a0) * (f_de2[2] - f_de1[2]), 
                            self.PACG[0] * (f_de1[2] + f_de2[2]),
                            self.PACG[1] * np.sin(self.a0) * (f_de2[2] - f_de1[2])])

        m_tot = m_torque + m_wing + m_delta

        return f_tot, m_tot


    def dyn(self, x, u):
        G = 9.81
        MASS = self.MASS
        INERTIA = np.diag(self.INERTIA)

        # state demultiplexing
        quat = x[3:7]
        vel = x[7:10]
        rot = x[10:13]

        Fb, Mb = self.force(vel, u)
        dvdt = np.array([0, 0, G]) + 1/MASS * quatrot(Fb, quatinv(quat))
        drdt = np.linalg.inv(INERTIA) @ (Mb - np.cross(rot, INERTIA@rot))

        # kinematic equations
        dpdt = vel
        dqdt = 1/2 * quatmul(quat, np.concatenate([[0], rot]))

        return np.concatenate([dpdt, dqdt, dvdt, drdt])

    def trim(self, vh, vz):
        x = np.zeros(13)
        x[7] = vh
        x[9] = vz

        def func(y):
            dx, de, theta = y
            x[3:7] = np.array([np.cos(theta/2), 0, np.sin(theta/2), 0])
            u = np.array([-dx, dx, de, de])
            dq_dt = self.dyn(x, u)
            return dq_dt[7], dq_dt[9], dq_dt[11] 

        y0 = np.array([1000, 0, 0])
        y = fsolve(func, y0)
        return y  # vh, vz, tangage

    def step(self, x, u, dt):
        func = lambda t, s: self.dyn(s, u)
        sol = solve_ivp(func, (0, dt), x, method='RK45', max_step=0.01)
        return sol.y.T[-1]

    def sim(self, t_span, x0, fctrl):
        func = lambda t, s: self.dyn(s, fctrl(t, s))
        def hit_ground(t, s): return -s[2]
        hit_ground.terminal = True
        hit_ground.direction = -1
        sol = solve_ivp(func, t_span, x0, method='RK45', max_step=0.01, events=hit_ground)
        return sol
