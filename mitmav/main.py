import numpy as np
from rotation import *
import matplotlib.pyplot as plt
from model import Mav

def upulse(t, t0, tf):
    u = 0 if (t<t0 or t>=tf) else 1
    return u

def ustep(t, t0) :
    u = 0 if t<t0 else 1
    return u

def uramp(t, t0) :
    u = 0 if t<t0 else (t - t0)
    return u

if __name__ == "__main__":
    uav = Mav()

    vh = 0
    vz = 0
    cap = 0
    omega = 0

    dx, de, theta = uav.trim(vh, vz)
    theta = (theta + np.pi) % (2*np.pi) - np.pi
    print(dx, de, theta*57.3)

    pos = np.array([0, 0, -10])
    ang = np.array([0, theta, cap])
    vel = np.array([vh * np.cos(cap), vh * np.sin(cap), vz])
    rot = np.array([0, 0, 0])
    quat = eul2quat(ang)

    # x0 = np.concatenate([pos, quat, vel, rot])
    x0 = np.concatenate([pos, quat, vel, rot])
    u0 = np.array([-dx, dx, de, de])

    def ctl(t, s):
        ddx = (upulse(t, 5, 6) - upulse(t, 6, 7))*dx*0.0
        qt = np.array([1/np.sqrt(2), 0, 1/np.sqrt(2), 0])
        q = s[3:7]
        dde = 0.0 * (q - qt)[2]
        u = u0 + np.array([ddx, ddx, dde, dde])
        return u

    sim = uav.sim((0, 60), x0, ctl)

    fig, axs = plt.subplots(2, 2)

    ax = axs[0][0]
    ax.plot(sim.t, sim.y[0:3].T, label=('x','y','z'))
    ax.legend()
    ax.grid()
    ax.set_title(label='positions')

    ax = axs[1][0]
    quat = sim.y[3:7].T
    # ax.plot(sim.t, quat, label=('q0', 'q1', 'q2', 'q3'))
    # ax.set_title(label='quaternion')
    eul = np.apply_along_axis(quat2eul, 1, quat) * rad2deg
    ax.plot(sim.t, eul, label=('phi', 'theta', 'psi'))
    ax.set_title(label='euler angles')
    ax.legend()
    ax.grid()

    ax = axs[0][1]
    ax.plot(sim.t, sim.y[7:10].T, label=('u','v','w'))
    ax.legend()
    ax.grid()
    ax.set_title(label='vitesses')

    ax = axs[1][1]
    ax.plot(sim.t, sim.y[10:13].T, label=('p','q','r'))
    ax.legend()
    ax.grid()
    ax.set_title(label='rotations')
    plt.show()
