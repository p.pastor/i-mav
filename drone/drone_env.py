import numpy as np
import gymnasium as gym
from gymnasium.envs.classic_control import utils

from rotation import *
from drone_model import Drone


class DroneEnv(gym.Env):

    def __init__(self, drone=Drone(), render_mode=None):

        self.drone = drone
        self.tau = 0.02  # seconds between state updates
        
        # Angle at which to fail the episode
        self.state = np.zeros(13)
        self.target = np.zeros(13)
        self.obs = np.zeros(13)
        
        self.pos_threshold = np.array([1., 1., 1.])
        self.quat_threshold = np.ones(4)
        self.vel_threshold = np.finfo(np.float32).max * np.ones(3)
        self.rot_threshold = np.finfo(np.float32).max * np.ones(3)

        high = np.concatenate(
            [
                self.pos_threshold,
                self.quat_threshold,
                self.vel_threshold,
                self.rot_threshold
            ],
            dtype=np.float32,
        )

        self.observation_space = gym.spaces.Box(-high, high, dtype=np.float32)
        self.action_space = gym.spaces.Box(0, 500, shape=(4,), dtype=np.float32)

        self.steps_beyond_terminated = None

    def observation(self):
        d_pos = self.state[0:3] - self.target[0:3]
        d_quat = quatmul(quatinv(self.target[3:7]), self.state[3:7])
        d_vel = self.state[7:10] - self.target[7:10]
        d_rot = self.state[10:13] - self.target[10:13]
        obs = np.concatenate([d_pos, d_quat, d_vel, d_rot])
        return obs

    def reward(self):
        dist_pos = np.linalg.norm(self.obs[0:3])
        dist_quat = quatdist(self.state[3:7], self.target[3:7])
        terminated = (dist_pos + dist_quat) > 1
        reward = 1 - (dist_pos + dist_quat) if not terminated else -1  
        return reward, terminated

    def reset(self, seed=None, options=None):
        super().reset(seed=seed)
        # Note that if you use custom reset bounds, it may lead to out-of-bound
        # state/observations.
        low, high = utils.maybe_parse_reset_bounds(options, -0.1, 0.1)  
        self.state = self.target + np.array([0.1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        self.obs = self.observation()
        return self.obs, {}

    def step(self, action):
        self.state = self.drone.step(self.state, action, self.tau)
        self.obs = self.observation()
        reward, terminated = self.reward()
        return self.obs, reward, terminated, False, {}

    def render(self, mode='human'):
        pass
