import numpy as np
from stable_baselines3 import SAC

from drone_model import Drone
from drone_env import DroneEnv


if __name__ == "__main__":

    drone_env = DroneEnv()
    drone_env.target = np.array([0, 0, -20, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0])

    model = SAC("MlpPolicy", drone_env, verbose=1)
    model.learn(total_timesteps=20000, log_interval=10)
    model.save("sac_drone_2")

