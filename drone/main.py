import numpy as np
import matplotlib.pyplot as plt

from drone_model import Drone
from drone_env import DroneEnv

deg2rad = np.pi/180
rad2deg = 1 / deg2rad

def upulse(t, t0, tf):
    u = 0 if (t<t0 or t>=tf) else 1
    return u

def ustep(t, t0) :
    u = 0 if t<t0 else 1
    return u

def uramp(t, t0) :
    u = 0 if t<t0 else (t - t0)
    return u


if __name__ == "__main__":
    drone_env = DroneEnv()

    vh = 0
    vz = 0
    n0 = drone_env.drone.trim(vh, vz)
    print(n0)
    
    drone_env.target = np.array([0, 0, -20, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0])
    drone_env.reset()
    x0 = drone_env.state
    print(x0)

    def ctl(t, s):
        dn = (upulse(t, 1, 2) - upulse(t, 2, 3))*np.linalg.norm(n0)*np.array([0.1, 0.1, 0.1, 0.1])
        n = n0 + dn
        return n

    sim = drone_env.drone.sim((0, 20), x0, ctl)

    fig, axs = plt.subplots(2, 2)

    ax = axs[0][0]
    ax.plot(sim.t, sim.y[0:3].T, label=('x','y','z'))
    ax.legend()
    ax.grid()
    ax.set_title(label='positions')

    ax = axs[1][0]
    quat = sim.y[3:7].T
    ax.plot(sim.t, quat, label=('w', 'x', 'y', 'z'))
    ax.legend()
    ax.grid()
    ax.set_title(label='quaternion')

    ax = axs[0][1]
    ax.plot(sim.t, sim.y[7:10].T, label=('u','v','w'))
    ax.legend()
    ax.grid()
    ax.set_title(label='vitesses')

    ax = axs[1][1]
    ax.plot(sim.t, sim.y[10:13].T, label=('p','q','r'))
    ax.legend()
    ax.grid()
    ax.set_title(label='rotations')
    plt.show()
