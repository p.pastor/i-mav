import numpy as np
import yaml
from scipy.optimize import fsolve
from scipy.integrate import solve_ivp
from rotation import *

class Drone:

    def __init__(self, file="drone.yaml"):
        stream = open(file, "r")
        drone = yaml.load(stream, Loader=yaml.Loader)
        self.NAME = drone['NAME']
        self.RHO = drone['RHO']
        self.G = drone['G']
        self.MASS = drone['MASS']
        self.INERTIA = np.diag(drone["INERTIA"])
        self.P_R_CG = np.array(drone["P_R_CG"])
        self.PROP_KP = drone['PROP_KP']
        self.PROP_KM = drone['PROP_KM']

    def dyn(self, x, n):

        G = self.G
        MASS = self.MASS
        INERTIA = self.INERTIA
        P_R_CG = self.P_R_CG
        KP = self.PROP_KP
        KM = self.PROP_KM

        T = KP * n**2
        N = KM * n**2
        u = np.array([sum(T), P_R_CG*(T[3]-T[2]), P_R_CG*(T[0]-T[1]), N[2]+N[3]-N[0]-N[1]])

        # state demultiplexing
        quat = x[3:7]
        vel = x[7:10]
        rot = x[10:13]

        # linear velocity derivative
        dvdt = quatrot(quatinv(quat), np.array([0, 0, -u[0]])) / MASS + np.array([0, 0, G])

        # angular velocity derivative
        drdt = np.linalg.inv(INERTIA) @ (u[1:4] - np.cross(rot, INERTIA@rot))

        # kinematic equations
        dpdt = vel
        dqdt = 0.5 * quatmul(quat, vect2quat(rot))

        return np.concatenate([dpdt, dqdt, dvdt, drdt])

    def trim(self, vh, vz):
        x = np.zeros(13)
        x[3:7] = np.array([1, 0, 0, 0])
        x[7] = vh
        x[9] = vz

        def func(n):
            dx = self.dyn(x, n)
            return dx[9], dx[10], dx[11], dx[12]

        y0 = np.array([100, 100, 100, 100])
        y = fsolve(func, y0)
        return y

    def step(self, x, n, dt):
        func = lambda t, s: self.dyn(s, n)
        sol = solve_ivp(func, (0, dt), x, method='RK45', max_step=0.01)
        return sol.y.T[-1]

    def sim(self, t_span, x0, fctrl):
        func = lambda t, s: self.dyn(s, fctrl(t, s))
        sol = solve_ivp(func, t_span, x0, method='RK45', max_step=0.01)
        return sol

