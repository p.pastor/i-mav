import numpy as np
import gymnasium as gym
from gymnasium.envs.classic_control import utils
from mavion_model import Mavion
from rotation import quatinv, quatmul

class MavionEnv(gym.Env):

    def __init__(self, mavion=Mavion(), render_mode=None):

        self.mavion = mavion
        self.tau = 0.2  # seconds between state updates

        # Angle at which to fail the episode
        self.pos_threshold = np.array([10, 10, 10])
        self.quat_threshold = np.array([1, 1, 1, 1])
        self.vel_threshold = np.finfo(np.float32).max * np.ones(3)
        self.rot_threshold = np.finfo(np.float32).max * np.ones(3)

        high = np.concatenate([
                self.pos_threshold,
                self.quat_threshold,
                self.vel_threshold,
                self.rot_threshold
            ],dtype=np.float32)
        self.observation_space = gym.spaces.Box(-high, high, dtype=np.float32)

        action_max = np.array([1, 1, 1, 1])
        action_min = np.array([0, 0, -1, -1])
        self.action_space = gym.spaces.Box(action_min, action_max, dtype=np.float32)
        self.action_range = np.array([mavion.MAX_ROTOR_SPD, mavion.MAX_ROTOR_SPD, mavion.MAX_FLAP_DEF, mavion.MAX_FLAP_DEF])

        self.state = np.zeros(13)
        self.target = np.zeros(13)
        self.obs = np.zeros(13)

    def observation(self):
        obs = np.zeros(13)
        obs = self.state - self.target
        obs[3:7] = quatmul(quatinv(self.target[3:7]), self.state[3:7])
        return obs

    def distance(self, s, t):
        d_pos = np.linalg.norm(t[0:3] - s[0:3]) # distance euclidienne
        d_quat = 2 * np.arccos(min(1, sum(t[3:7] * s[3:7]))) / np.pi # compris entre 0 et 1
        d_vel = np.linalg.norm(t[7:10] - s[7:10]) # distance euclidienne
        d_rot = np.linalg.norm(t[10:13] - s[10:13]) # distance euclidienne
        return d_pos + d_quat*10 + d_vel*0 + d_rot*0

    def reward(self):
        dist = self.distance(self.state, self.target)
        terminated = dist > 10
        reward = 10 - dist if not terminated else -10
        return reward, terminated

    def step(self, action):
        x = self.state
        u = action * self.action_range
        self.state  = self.mavion.step(x, u, np.zeros(3), self.tau)
        self.obs = self.observation()
        reward, terminated = self.reward()
        return self.obs, reward, terminated, False, {}

    def reset(self, seed=None, options=None):
        super().reset(seed=seed)
        low, high = utils.maybe_parse_reset_bounds(options, -1, 1)
        delta = self.np_random.uniform(low=low, high=high, size=(3,))
        self.state = self.target + np.concatenate([delta, np.zeros(10)])
        self.obs = self.observation()
        return self.obs, {}
