import numpy as np
from stable_baselines3 import SAC
import matplotlib.pyplot as plt
from mavion_model import Mavion
from mavion_env import MavionEnv
from rotation import *

def upulse(t, t0, tf):
    u = 0 if (t<t0 or t>=tf) else 1
    return u

def ustep(t, t0) :
    u = 0 if t<t0 else 1
    return u

def uramp(t, t0) :
    u = 0 if t<t0 else (t - t0)
    return u

if __name__ == "__main__":

    mav_env = MavionEnv()

    vh = 12
    vz = 0
    cap = 0/rad2deg
    omega = 0

    target = [vh, vz]

    dx, de, theta = mav_env.mavion.trim(target)
    theta = (theta + np.pi) % (2*np.pi) - np.pi
    print(dx, de, theta*rad2deg)

    pos = np.array([0, 0, -20])
    ang = np.array([0, theta, cap])
    vel = np.array([vh * np.cos(cap), vh * np.sin(cap), vz])
    rot = np.array([0, 0, 0])
    quat = eul2quat(ang)

    x0 = np.concatenate([pos, quat, vel, rot])
    
    pos_target = np.array([0, 0, -20])
    eul_target = np.array([0, 90, 0])*deg2rad
    quat_target = eul2quat(eul_target)
    vel_target = np.zeros(3)
    rot_target = np.zeros(3)

    mav_env.target = np.concatenate([pos_target, quat_target, np.zeros(3), np.zeros(3)])
    mav_agent = SAC.load("sac_mavion_2", env=mav_env)
    mav_env.reset()
    x0 = mav_env.state

    def ctl(t, s):
        act, _ = mav_agent.predict(s, deterministic=True)
        # u = act * mav_env.action_range
        u = np.array([dx, dx, de, de])
        return u

    def wnd(t, s):
        w = np.zeros(3)
        return w 

    print(x0, ctl(0, x0))

    sim = mav_env.mavion.sim((0, 120), x0, ctl, wnd)

    fig, axs = plt.subplots(2, 2)

    ax = axs[0][0]
    ax.plot(sim.t, sim.y[0:3].T, label=('x','y','z'))
    ax.legend()
    ax.grid()
    ax.set_title(label='positions')

    ax = axs[1][0]
    quat = sim.y[3:7].T
    ax.plot(sim.t, quat, label=('q0', 'q1', 'q2', 'q3'))
    ax.set_title(label='quaternion')
    # eul = np.apply_along_axis(quat2eul, 1, quat) * rad2deg
    # ax.plot(sim.t, eul, label=('phi', 'theta', 'psi'))
    # ax.set_title(label='euler angles')
    ax.legend()
    ax.grid()

    ax = axs[0][1]
    ax.plot(sim.t, sim.y[7:10].T, label=('Vn','Ve','Vv'))
    ax.legend()
    ax.grid()
    ax.set_title(label='vitesses dans le rep. terre')

    ax = axs[1][1]
    ax.plot(sim.t, sim.y[10:13].T, label=('p','q','r'))
    ax.legend()
    ax.grid()
    ax.set_title(label='rotations')
    plt.show()
