import numpy as np
from stable_baselines3 import SAC
from mavion_env import MavionEnv
from rotation import *


if __name__ == "__main__":

    pos_target = np.array([0, 0, -20])
    eul_target = np.array([0, 90, 0])*deg2rad
    quat_target = eul2quat(eul_target)
    vel_target = np.zeros(3)
    rot_target = np.zeros(3)

    env = MavionEnv()
    env.target = np.concatenate([pos_target, quat_target, np.zeros(3), np.zeros(3)])
    print(env.target)
    obs = env.reset()
    print(env.state)
    print(obs)

    model = SAC("MlpPolicy", env, verbose=1)
    model.learn(total_timesteps=20000, log_interval=10)
    model.save("sac_mavion_2")
